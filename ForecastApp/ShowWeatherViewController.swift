//
//  ViewController.swift
//  ForecastApp
//
//  Created by Roman on 6/19/18.
//  Copyright © 2018 Roman. All rights reserved.
//

import UIKit

class ShowWeatherViewController: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var showForecastTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showForecastTableView.register(UINib(nibName: String(describing: ShowWeatherForecastTableViewCell.self), bundle: nil),
                                       forCellReuseIdentifier: ShowWeatherForecastTableViewCell.cellIdentifier)
        showForecastTableView.tableFooterView = UIView()
    }
    
    func numberOfSections (in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ShowWeatherForecastTableViewCell.cellIdentifier, for: indexPath) as! ShowWeatherForecastTableViewCell
        
        return cell
    }

}

