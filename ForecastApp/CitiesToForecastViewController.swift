//
//  CitiesToForecastTableViewController.swift
//  ForecastApp
//
//  Created by Roman on 6/20/18.
//  Copyright © 2018 Roman. All rights reserved.
//

import UIKit

class CitiesToForecastViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: - Properties
    var citiesToShowForecast = [String]()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var enterCityTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: String(describing: String(describing: CityToForecastTableViewCell.self)), bundle: nil),
                           forCellReuseIdentifier: CityToForecastTableViewCell.cellIdentifier)
        
        tableView.tableFooterView = UIView()
        
        enterCityTextField.delegate = self
    }
    
     func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return citiesToShowForecast.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CityToForecastTableViewCell.cellIdentifier, for: indexPath) as! CityToForecastTableViewCell
        
        return cell
    }
}
