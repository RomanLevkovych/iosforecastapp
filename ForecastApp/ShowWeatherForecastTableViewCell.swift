//
//  ShowWeatherForecastTableViewCell.swift
//  ForecastApp
//
//  Created by Roman on 6/19/18.
//  Copyright © 2018 Roman. All rights reserved.
//

import UIKit

class ShowWeatherForecastTableViewCell: UITableViewCell {

    //MARK: - Objects
    
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - Static fields
    static let cellIdentifier = "ShowWeatherInCity"
    
}
