//
//  CityToForecastTableViewCell.swift
//  ForecastApp
//
//  Created by Roman on 6/19/18.
//  Copyright © 2018 Roman. All rights reserved.
//

import UIKit

class CityToForecastTableViewCell: UITableViewCell {

    //MARK: - Objects
    @IBOutlet weak var cityNameToShowForecast: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - static fields
    static let cellIdentifier = "CityToShowForecast"
}
