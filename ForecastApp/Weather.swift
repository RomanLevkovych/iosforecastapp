//
//  Weather.swift
//  ForecastApp
//
//  Created by Roman on 6/23/18.
//  Copyright © 2018 Roman. All rights reserved.
//

import Foundation

class WeatherInCity {
    let cityToShowWeather: String
    var windSpeed: Double
    let rainProbability: Double
    var clouds: Int
    var humidity: Int
    var pressure: Int
    
    init(city: String,
         wind: Double,
         rain: Double,
         clouds: Int,
         humidity: Int,
         pressure: Int) {
        self.cityToShowWeather = city
        self.windSpeed = wind
        self.rainProbability = rain
        self.clouds = clouds
        self.humidity = humidity
        self.pressure = pressure
    }
}
